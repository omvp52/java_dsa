/*E D C B A
  E D C B
  E D C 
  E D
  E
 */

	  
import java.io.*;
	  class Demo{
		  public static void main (String[]args)throws IOException{
		  BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
	          System.out.println("Enter the number of rows:");
		  int N=Integer.parseInt(br.readLine());
	
		    for(int i=1;i<=N;i++){
		       char ch='E';													                            for(int j=1;j<=N-i+1;j++){
                          System.out.print((ch--)+ " ");
		       }
                     System.out.println();
		    }
		  }
	  }
