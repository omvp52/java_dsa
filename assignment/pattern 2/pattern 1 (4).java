/* 0   1   1   2
   3   5   8   13
  21   34  55  89
  */

import java.util.*;
class Demo{
	public static void main(String[]args){
		Scanner sc=new Scanner(System.in);
		int row=sc.nextInt();
		int a=0;
		int b=1;
		for(int i=1;i<=row;i++){
			for(int j=1;j<=row+1;j++){
				System.out.print(a+" ");
				int temp=a+b;
				a=b;
				b=temp;
			}
			System.out.println();
		}
	}
}
