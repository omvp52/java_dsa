/* E D C B A
   E D C B
   E D C
   E D
   E 
*/


import java.io.*;
class Demo{
	public static void main (String[]args)throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter the number of rows:");
		int N=Integer.parseInt(br.readLine());
		int row=1;
		   for(int i=1;i<=N;i++){
			   for(int j=1;j<=N;j++){
			   System.out.print((row*row-1)+" ");
			   row++;
		   }
			   System.out.println();
		   }
	}
}
