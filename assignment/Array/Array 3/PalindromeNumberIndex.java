/*
 WAP to find a palindrome number from an array and return its index.
Take size and elements from the user
Input: 10 25 252 36 564
Output: Palindrome no 252 found at index: 2*/
import java.io.*;
class PalindromeNumberIndex {
        public static void main(String ar[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enetr Total number of Elements :");
                int num = Integer.parseInt(br.readLine());
                int arr[] = new int[num];
                System.out.println("Enetr Elements :");
                for(int i=0; i<arr.length; i++) {
                        arr[i]=Integer.parseInt(br.readLine());
                }


                for(int i=0; i<arr.length; i++) {
                        int rev = 0;
                        int ele = arr[i];
                        while(ele!=0) {
                                int rem = ele%10;
                                rev= rev*10+rem;
                                ele = ele/10;
                        }
                        if(arr[i]==rev){
                                System.out.println("Palindrome no "+arr[i]+" found at index: "+i);
                        }
                }
        }
}
