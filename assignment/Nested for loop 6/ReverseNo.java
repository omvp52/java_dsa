/*
 Write a program to take range as input from the user and print the reverse of all numbers. ( Take a
start and end number from a user )
Input: Enter start: 100
Enter end: 200
*/
import java.io.*;
class ReverseNo {
        public static void main(String ar[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter start limit");
                int start = Integer.parseInt(br.readLine());
                System.out.println("Enter end limit");
                int end =  Integer.parseInt(br.readLine());

                for(int i=start; i<=end; i++) {
                        int num = i;
                        int rev =0;
                        while(num!=0) {
                                int rem = num%10;
                                rev = rev*10+rem;
                                num = num/10;
                        }
                        System.out.print(rev+" ");
                }
        }
}
