/*
 Write a program to take range as input from the user and print Armstrong numbers. ( Take a start and
end number from a user )
Input: Enter start: 1
Enter end: 1650
*/
import java.io.*;
class ArmstrongNo {
        public static void main(String ar[])throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter start limit");
                int start = Integer.parseInt(br.readLine());
                System.out.println("Enter end limit");
                int end =  Integer.parseInt(br.readLine());

                for(int i=start; i<=end; i++){
                        int num=i;
                        int add = 0;
                        while(num!=0) {
                                int rem = num%10;
                                add= add+(rem*rem*rem);
                                num=num/10;
                        }
                        if(i==add) {
                                System.out.print(i+" ");
                        }
                }
        }
}