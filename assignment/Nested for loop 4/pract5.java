/*
A B C D
B C D
C D
D
*/
class C2W {
        public static void main (String ar[]) {
                int row = 4;
                for (int i=1; i<=row; i++) {
                        char ch= 'A';
                        for(int k=1; k<=(i-1); k++) {
                                ch++;
                        }
                        for(int j=1; j<=(row-i+1); j++) {
                                System.out.print(ch++ +" ");
                        }
                        System.out.println();
                }
        }
}

/*
class Pattern15{

        public static void main(String args[]){

                int n = 4;
                char ch = 'A';

                for (int i =1; i<=n; i++){

                        for(int j =1; j<=n-i+1; j++){

                                System.out.print(ch+" ");
                                ch++;
                        }
                        int all = ch - (n-i);
                         ch = (char)all;        //A B C D E F G H
                         System.out.println();
                }
        }
}
*/