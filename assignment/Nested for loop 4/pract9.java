/*
1
8 9
27 16 125
64 25 216 49
*/

class C2W {
        public static void main (String ar[]) {
                int row=4;
                int num=1;
                int a;
                for(int i=1; i<=row; i++) {
                        a=i;
                        for(int j=1; j<=i; j++) {
                                if (j%2==1){
                                        System.out.print(a*a*a +" ");
                                        a++;
                                }
                                else{
                                        System.out.print(a*a +" ");
                                        a++;
                                }
                        }
                        System.out.println();
                }
        }
}