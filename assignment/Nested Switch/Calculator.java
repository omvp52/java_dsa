import java.io.*;
class PatternDemo {
        public static void main(String ar[])throws IOException {
                BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
                System.out.println("enter the 1st number: ");
                int num1= Integer.parseInt(br.readLine());
                System.out.println("enter the 2nd number: ");
                int num2= Integer.parseInt(br.readLine());
                System.out.println("enter choice number");
                System.out.println("1.Addition");
                System.out.println("2.Subtraction");
                System.out.println("3.Multiplication");
                System.out.println("4.Division");
                int choice = Integer.parseInt(br.readLine());
                switch(choice) {
                        case 1: {
                                        int ans= num1+num2;
                                        System.out.println("Addition = "+ans);
                                        break;
                        }
                        case 2: {
                                        int ans= num1-num2;
                                        System.out.println("Subtraction = "+ans);
                                        break;
                        }
                        case 3: {
                                        int ans= num1*num2;
                                        System.out.println("Multiplication = "+ans);
                                        break;
                        }
                        case 4: {
                                        int ans= num1/num2;
                                        System.out.println("Division = "+ans);
                        }
                        default : {
                                        System.out.println("invalid choice");
                                        break;
                        }
                }
        }
}
