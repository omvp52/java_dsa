class EqualsIgnoreCaseDemo {
	        public static void main (String args []) {
			  String str1 = "Om";
		          String str2 = "Om";
			  String str3 = "OmPatil";
               System.out.println(str1.equalsIgnoreCase(str2)); //true
	       System.out.println(str2.equalsIgnoreCase(str3)); //false
	       System.out.println(str3.equalsIgnoreCase(str3)); //true
         }
}
