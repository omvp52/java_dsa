class CompareToIgnoreCase {
	        public static void main (String args []) {
		     String str1 = "Om";
		     String str2 = "Om";
		     String str3 = "Ompatil";
	     System.out.println(str1.compareToIgnoreCase(str2)); //0
	     System.out.println(str1.compareToIgnoreCase(str2)); //0
	     System.out.println(str1.compareToIgnoreCase(str3)); //-5
             System.out.println(str2.compareToIgnoreCase(str1)); //0
	     System.out.println(str3.compareToIgnoreCase(str1)); // 5
	   }
}

