class CompareToDemo {
	        public static void main (String[]args) {
			String str1 = "Om";
		        String str2 = "Om";
	                String str3 = "Om";
			String str4 = "OmPatil";
			System.out.println(str1.compareTo(str2));//0
		        System.out.println(str1.compareTo(str4));//-5
		        System.out.println(str2.compareTo(str1));//0
		        System.out.println(str3.compareTo(str1));//0
                        System.out.println(str4.compareTo(str1));//5
		}
}
