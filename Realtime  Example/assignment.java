class Amazon{
	static String product="Puma";
	int noOfShoes=10;
	static int price=4000;

	void availability(){
		System.out.println("Brand is:" + product);
		System.out.println("Shoes Number:" + noOfShoes);
		System.out.println("product price:" + price);
	}
}
class MainDemo{
	public static void main(String[]args){
		Amazon az1=new Amazon();
		Amazon az2=new Amazon();

		az1.availability();
		az2.availability();

		az2.product="Reebok";
		az2.noOfShoes=8;
		az2.price=2000;
		
		az1.availability();
		az2.availability();
	}
}
